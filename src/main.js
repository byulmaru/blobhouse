const express = require('express');
const cookieParser = require('cookie-parser');
const rs = require('randomstring');
const path = require('path');
const axios = require('axios');
const fs = require('fs');
const http = require('http');
const sio = require('socket.io');

require('express-async-errors');

const domain = process.env.NODE_ENV === 'production' ? 'http://blobhouse.planet.moe' : 'http://localhost:4001';
const emoji = {};
require('../emoji-data.json').forEach((e) => {
    emoji[e.shortcode] = e.url;
});

const app = express();
const server = http.createServer(app);
const io = sio(server);

app.use(cookieParser());

let sessionStore = {};
let appStore = require('../app-data.json') || {};

function saveApp() {
    fs.writeFile(path.join(__dirname, '../app-data.json'), JSON.stringify(appStore), () => {});
}

app.get('/', async(req, res) => {
    if(!req.cookies || !sessionStore[req.cookies.session]) {
        res.sendFile(path.join(__dirname, 'views', 'login.html'));
    }
    else {
        res.sendFile(path.join(__dirname, 'views', 'blobhouse.html'));
    }
});
app.get('/login', async(req, res) => {
    const url = new URL(`https://${req.query.domain}`);
    if(!appStore[url.host]) {
        const nodeInfoLink = (await axios.get(`${url.origin}/.well-known/nodeinfo`)).data.links
        .find((link) => link.rel === 'http://nodeinfo.diaspora.software/ns/schema/2.0');
        const nodeInfo = (await axios.get(nodeInfoLink.href)).data;
        switch(nodeInfo.software.name) {
            case 'mastodon':
                let app = (await axios.post(`${url.origin}/api/v1/apps`, {
                    client_name: 'Blobhouse',
                    redirect_uris: `${domain}/login/${url.host}`
                })).data;
                appStore[url.host] = {
                    type: 'mastodon',
                    client_id: app.client_id,
                    client_secret: app.client_secret,
                    redirect_uri: `${url.origin}/oauth/authorize?client_id=${app.client_id}&response_type=code&redirect_uri=${domain}/login/${url.host}`
                };
                saveApp();
                break;
        }
    }
    res.redirect(appStore[url.host].redirect_uri);
});
app.get('/login/:domain', async(req, res) => {
    const url = new URL(`https://${req.params.domain}`);
    switch(appStore[url.host].type) {
        case 'mastodon':
            let tokenData = (await axios.post(`${url.origin}/oauth/token`, {
                client_id: appStore[url.host].client_id,
                client_secret: appStore[url.host].client_secret,
                redirect_uri: `${domain}/login/${url.host}`,
                code: req.query.code,
                grant_type: 'authorization_code'
            })).data;
            let userData = (await axios.get(`${url.origin}/api/v1/accounts/verify_credentials`, {
                headers: {
                    Authorization: `Bearer ${tokenData.access_token}`
                }
            })).data;
            let sessionKey = rs.generate();
            sessionStore[sessionKey] = {
                name: userData.display_name,
                token: tokenData.access_token
            };
            res.cookie('session', sessionKey);
            console.log(`${userData.display_name} Logged in`);
            break;
    }
    res.redirect('/');
});
app.get('/emoji', async(req, res) => {
    res.json(Object.entries(emoji));
});

app.use(async(err, req, res, next) => {
    console.error(err);
    res.send('아무튼에러!!');
});

io.on('connection', (socket) => {
    socket.on('emoji', (data) => {
        if(sessionStore[data.session]) {
            io.emit('emoji', {url: emoji[data.id], name: sessionStore[data.session].name});
        }
    });
})

server.listen(4001);
